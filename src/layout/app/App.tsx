import { useLazyQuery, useQuery } from "@apollo/client";
import React, { useState } from "react";

import ClubPicker from "../../components/club-picker/club-picker.view";
import CrossesOverview from "../../components/crosses-overview/crosses-overview.view";
import Loader from "../../components/loader/loader.view";
import { GET_CLUB, GET_CLUBS } from "../../queries/clubs.queries";

import {
  BasicClub,
  ClubData,
  ClubListData,
  GET_CLUB_PROPS,
} from "../../types/club.types";
import { EventType } from "../../types/event.types";

import styles from "./app.module.css";

function App() {
  /* Hooks n State */
  const [_isPickingClub, _setIsPickingClub] = useState<boolean>(true);

  const _clubs = useQuery<ClubListData>(GET_CLUBS);
  const [_getClub, _clubData] = useLazyQuery<ClubData, GET_CLUB_PROPS>(
    GET_CLUB
  );

  /* Listeners */
  function _onClubSelect(club: BasicClub) {
    _setIsPickingClub(false);

    _getClub({
      variables: { clubName: club.club_name, eventType: EventType.PASS },
    });
  }

  function _onActiveClubIconClick(club: BasicClub) {
    _setIsPickingClub(true);
  }

  /* Render */
  return (
    <div className={styles.app}>
      <Loader visible={_clubs.loading || _clubData.loading} />
      <ClubPicker
        visible={_isPickingClub}
        clubs={_clubs.data?.clubs}
        onClubSelect={_onClubSelect}
      />
      {_clubData.data?.club && (
        <CrossesOverview
          club={_clubData.data.club}
          onIconClick={_onActiveClubIconClick}
        />
      )}
    </div>
  );
}

export default App;
