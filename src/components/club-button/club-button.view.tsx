import cn from "classnames";
import { BasicClub } from "../../types/club.types";

import styles from "./club-button.module.css";

interface ClubButtonProps {
  showName?: boolean;
  club: BasicClub;
  onClick: (club: BasicClub) => void;
  size: "small" | "large";
}

function ClubButton(props: ClubButtonProps): JSX.Element {
  const _buttonClasses = cn(styles.club, styles[`club-${props.size}`]);
  return (
    <button
      className={_buttonClasses}
      key={`clubpicker-${props.club.club_name}`}
      onClick={() => props.onClick(props.club)}
    >
      <span className={styles.clubImageWrapper}>
        <span
          className={styles.clubImage}
          style={{ backgroundImage: `url(${props.club.image}` }}
        ></span>
      </span>
      {props.showName && (
        <span className={styles.clubName}>{props.club.club_name}</span>
      )}
    </button>
  );
}

export default ClubButton;
