import { useEffect, useState } from "react";
import { PlayerWithCrosses } from "../../types/player.types";
import { CrossesCountInfo } from "../../types/event.types";

import styles from "./players-list.module.css";
import getCrossesSuccessRate from "../../helpers/get-crosses-success-rate.helper";
import CrossPositions from "../cross-positions/cross-positions.view";

interface PlayersListItemProps {
  player: PlayerWithCrosses;
}

function PlayersListItem(props: PlayersListItemProps): JSX.Element | null {
  const [_crossInfo, _setCrossInfo] = useState<CrossesCountInfo | null>(null);
  /* Hooks n State */
  useEffect(() => {
    const _tempCrossInfo = getCrossesSuccessRate(props.player.crosses);

    _setCrossInfo(_tempCrossInfo);
  }, [props.player]);

  /* Listeners */

  /* Render */
  if (!_crossInfo) return null;

  let _successRate = 0;
  if (_crossInfo.total.total) {
    _successRate = Math.round(
      (_crossInfo.total.successful / _crossInfo.total.total) * 100
    );
  }
  return (
    <tr
      className={styles.player}
      key={`players-list-${props.player.player_name}`}
    >
      <td>
        <div
          className={styles.playerImage}
          style={{ backgroundImage: `url(${props.player.image})` }}
        ></div>
      </td>
      <td className={styles.tdName}>
        <div className={styles.tdNameText}>{`${props.player.player_name}`}</div>
        <div className={styles.tdNameCrosses}>{`${
          _crossInfo.total.successful
        } successful cross${
          _crossInfo.total.successful !== 1 ? "es" : ""
        }`}</div>
      </td>
      <td>{_crossInfo.total.total}</td>
      <td>{`${_successRate}%`}</td>
      <td>
        <CrossPositions crossesInfo={_crossInfo} />
      </td>
    </tr>
  );
}

export default PlayersListItem;
