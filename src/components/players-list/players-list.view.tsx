import { PlayerWithCrosses } from "../../types/player.types";
import PlayersListItem from "./players-list-item.view";

import styles from "./players-list.module.css";

interface PlayersListProps {
  players: PlayerWithCrosses[];
}

function PlayersList(props: PlayersListProps): JSX.Element {
  return (
    <table className={styles.list}>
      <thead>
        <tr>
          <th>{"Player"}</th>
          <th>{""}</th>
          <th>{"# crosses"}</th>
          <th>{"Success rate"}</th>
          <th>{"Average position"}</th>
        </tr>
      </thead>
      <tbody>
        {props.players.map((player) => {
          return (
            <PlayersListItem
              key={`players-list-${player.player_name}`}
              player={player}
            />
          );
        })}
      </tbody>
    </table>
  );
}

export default PlayersList;
