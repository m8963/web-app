import { createRef, Fragment, useEffect, useState } from "react";
import { Layer, Stage, Circle, Line } from "react-konva";

import { PlayerWithCrosses } from "../../types/player.types";
import { Dimensions } from "../../types/pitch.types";
import { Event } from "../../types/event.types";
import calculatePoints from "../../helpers/calculate-points";
import CROSSES_TRESHOLD from "../../config/cross-tressholds.config";
import STRINGS from "../../config/strings";
import IMG_PITCH from "../../img/pitch.png";

import styles from "./pitch.module.css";

interface PitchProps {
  players: PlayerWithCrosses[];
}

function Pitch(props: PitchProps): JSX.Element {
  /* Hooks n State */
  const _containerNodeRef = createRef<HTMLDivElement>();
  const [_stageDimensions, _setStageDimensions] = useState<Dimensions>({
    width: 400,
    height: 600,
  });
  const [_highlightedEvent, _setHighlightedEvent] = useState<Event | null>(
    null
  );

  useEffect(() => {
    function _handleResize() {
      if (_containerNodeRef.current) {
        _setStageDimensions({
          height: _containerNodeRef.current.clientHeight,
          width: _containerNodeRef.current.clientWidth,
        });
      }
    }

    window.addEventListener("resize", _handleResize);

    return () => {
      window.removeEventListener("resize", _handleResize);
    };
  }, [_containerNodeRef]);

  /* Listeners */
  function _onShapeMouseOver(cross: Event) {
    _setHighlightedEvent(cross);
  }

  function _onShapeMouseLeave() {
    _setHighlightedEvent(null);
  }

  /* Render */
  return (
    <div className={styles.container} ref={_containerNodeRef}>
      <img className={styles.pitchImage} src={IMG_PITCH} alt="Pitch" />
      <div className={styles.canvas}>
        <Stage width={_stageDimensions.width} height={_stageDimensions.height}>
          {props.players.map((player) => {
            return (
              <Layer key={`canvas-player-${player.player_name}`}>
                {player.crosses.map((cross, index) => {
                  let _strokeColor = "#de8282";
                  if (cross.start_y <= CROSSES_TRESHOLD.LATE)
                    _strokeColor = "#de9e82";
                  if (cross.start_y < CROSSES_TRESHOLD.MID)
                    _strokeColor = "#deda82";

                  const _points = calculatePoints(cross, _stageDimensions);

                  let _circleFill = _strokeColor;
                  if (cross.pass?.receiving_player === STRINGS.OPPONENT)
                    _circleFill = "#374859";
                  const _key = cross._id;

                  return (
                    <Fragment key={_key}>
                      <Line
                        key={`${_key}-line`}
                        stroke={_strokeColor}
                        points={_points}
                        onClick={() => _onShapeMouseOver(cross)}
                        onMouseEnter={() => _onShapeMouseOver(cross)}
                        onMouseLeave={() => {
                          _onShapeMouseLeave();
                        }}
                        opacity={_highlightedEvent?._id === _key ? 1 : 0.6}
                      />
                      <Circle
                        key={`${_key}-circle`}
                        x={_points[2]}
                        y={_points[3]}
                        radius={4}
                        stroke={_strokeColor}
                        fill={_circleFill}
                        onClick={() => _onShapeMouseOver(cross)}
                        onMouseEnter={() => _onShapeMouseOver(cross)}
                        onMouseLeave={() => {
                          _onShapeMouseLeave();
                        }}
                      />
                    </Fragment>
                  );
                })}
              </Layer>
            );
          })}
        </Stage>
      </div>
      {_highlightedEvent && (
        <div className={styles.highlightedEvent}>
          <div className={styles.highlightedEventPlayer}>
            {_highlightedEvent.player_name}
          </div>
          <div className={styles.highlightedEventMatch}>
            <div className={styles.highlightedEventTeams}>
              {`${_highlightedEvent.match.home_team} - ${_highlightedEvent.match.away_team}`}
            </div>
            <div className={styles.highlightedEventScore}>
              {`${_highlightedEvent.match.home_team_score || 0} - ${
                _highlightedEvent.match.away_team_score || 0
              }`}
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default Pitch;
