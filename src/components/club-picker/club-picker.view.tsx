import { useRef } from "react";
import { CSSTransition } from "react-transition-group";

import { BasicClub } from "../../types/club.types";
import ClubButton from "../club-button/club-button.view";
import styles from "./club-picker.module.css";

interface ClubPickerProps {
  clubs: BasicClub[] | undefined;
  onClubSelect: (club: BasicClub) => void;
  visible: boolean;
}

function ClubPicker(props: ClubPickerProps): JSX.Element | null {
  /* Hooks n State */
  const _nodeRef = useRef(null);

  /* Render */
  if (!props.clubs) return null;
  return (
    <CSSTransition
      nodeRef={_nodeRef}
      in={props.visible}
      timeout={200}
      unmountOnExit
      classNames={{
        enterActive: styles.enterActive,
        enter: styles.enter,
        exitActive: styles.exitActive,
        exit: styles.exit,
      }}
    >
      <div className={styles.container} ref={_nodeRef}>
        <div className={styles.title}>{"Select a club"}</div>
        <div className={styles.clubs}>
          {props.clubs.map((club) => {
            return (
              <ClubButton
                club={club}
                key={`clubpicker-${club.club_name}`}
                onClick={props.onClubSelect}
                showName
                size="large"
              />
            );
          })}
        </div>
      </div>
    </CSSTransition>
  );
}

export default ClubPicker;
