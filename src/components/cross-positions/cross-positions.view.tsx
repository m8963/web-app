import cn from "classnames";
import { CrossesCountInfo } from "../../types/event.types";
import styles from "./cross-positions.module.css";

interface CrossPositionsProps {
  crossesInfo: CrossesCountInfo;
}

function CrossPositions(props: CrossPositionsProps): JSX.Element {
  return (
    <div className={styles.container}>
      {/* Yes, this can be written waaay more efficiently. But I don't feel like dealing with TypeScript limitations on mapping objects */}
      <div className={styles.visual}>
        {props.crossesInfo.early.total > 0 && (
          <div
            className={cn(styles.visualPart, styles.visualEarly)}
            style={{
              flex:
                props.crossesInfo.early.total / props.crossesInfo.total.total,
            }}
          ></div>
        )}
        {props.crossesInfo.mid.total > 0 && (
          <div
            className={cn(styles.visualPart, styles.visualMid)}
            style={{
              flex: props.crossesInfo.mid.total / props.crossesInfo.total.total,
            }}
          ></div>
        )}
        {props.crossesInfo.late.total > 0 && (
          <div
            className={cn(styles.visualPart, styles.visualLate)}
            style={{
              flex:
                props.crossesInfo.late.total / props.crossesInfo.total.total,
            }}
          ></div>
        )}
      </div>
      <div className={styles.text}>
        {props.crossesInfo.early.total > 0 && (
          <div
            className={styles.crossTypeText}
          >{`${props.crossesInfo.early.total} early`}</div>
        )}
        {props.crossesInfo.mid.total > 0 && (
          <div
            className={styles.crossTypeText}
          >{`${props.crossesInfo.mid.total} mid`}</div>
        )}
        {props.crossesInfo.late.total > 0 && (
          <div
            className={styles.crossTypeText}
          >{`${props.crossesInfo.late.total} late`}</div>
        )}
      </div>
    </div>
  );
}

export default CrossPositions;
