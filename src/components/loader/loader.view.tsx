import { useRef } from "react";
import { CSSTransition } from "react-transition-group";
import styles from "./loader.module.css";

import IMG_BALL from "../../img/icon/ball.svg";

interface LoaderProps {
  visible: boolean;
}
function Loader(props: LoaderProps): JSX.Element {
  /* Hooks n State */
  const _nodeRef = useRef(null);
  /* Render */
  return (
    <CSSTransition
      nodeRef={_nodeRef}
      in={props.visible}
      timeout={200}
      unmountOnExit
      classNames={{
        enterActive: styles.enterActive,
        enter: styles.enter,
        exitActive: styles.exitActive,
        exit: styles.exit,
      }}
    >
      <div className={styles.container} ref={_nodeRef}>
        <img className={styles.image} src={IMG_BALL} alt="Loader" />
      </div>
    </CSSTransition>
  );
}
export default Loader;
