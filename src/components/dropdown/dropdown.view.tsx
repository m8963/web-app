import { useState, useRef } from "react";
import { CSSTransition } from "react-transition-group";
import cn from "classnames";
import IMG_ARROW from "../../img/icon/arrow-down.svg";
import styles from "./dropdown.module.css";

interface DropdownProps {
  placeholder: string;
  options: string[];
  onChange: (value: string | null) => void;
  value: string | null;
}

function Dropdown(props: DropdownProps) {
  /* Hooks n State */
  const [_isOpened, _setIsOpened] = useState<boolean>(false);
  const _optionsNodeRef = useRef(null);

  /* Listeners */
  function _onInputClick() {
    _setIsOpened(!_isOpened);
  }

  function _onOptionClick(value: string | null) {
    _setIsOpened(false);
    props.onChange(value);
  }
  /* Render */
  const _label = props.value || props.placeholder;
  const _inputClasses = cn(styles.input, {
    [styles.inputOpened]: _isOpened,
  });
  return (
    <div className={styles.container}>
      <button className={_inputClasses} onClick={_onInputClick}>
        <span className={styles.inputLabel}>{_label}</span>
        <span className={styles.inputIcon}>
          <img className={styles.inputIconImage} src={IMG_ARROW} alt="-" />
        </span>
      </button>
      <CSSTransition
        in={_isOpened}
        timeout={200}
        unmountOnExit
        classNames={{
          enterActive: styles.optionsEnterActive,
          enter: styles.optionsEnter,
          exitActive: styles.optionsExitActive,
          exit: styles.optionsExit,
        }}
        nodeRef={_optionsNodeRef}
      >
        <div className={styles.options} ref={_optionsNodeRef}>
          <button
            className={styles.option}
            onClick={() => _onOptionClick(null)}
          >
            {props.placeholder}
          </button>
          {props.options.map((option) => {
            return (
              <button
                className={styles.option}
                key={`dropdodown-${option}`}
                onClick={() => _onOptionClick(option)}
              >
                {option}
              </button>
            );
          })}
        </div>
      </CSSTransition>
    </div>
  );
}

export default Dropdown;
