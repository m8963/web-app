import { useEffect, useState } from "react";
import { formatCrosses } from "../../helpers/format-crosses.helper";
import { BasicClub, Club } from "../../types/club.types";
import { CrossesData } from "../../types/event.types";

import ClubButton from "../club-button/club-button.view";
import Dropdown from "../dropdown/dropdown.view";

import Pitch from "../pitch/pitch.view";
import PlayersList from "../players-list/players-list.view";

import styles from "./crosses-overview.module.css";

interface CrossesOverviewProps {
  club: Club;
  onIconClick: (club: BasicClub) => void;
}

const PLAYERS_TO_SHOW = 3;

function CrossesOverview(props: CrossesOverviewProps): JSX.Element | null {
  /* Hooks n State */
  const [_crossesData, _setCrossesData] = useState<CrossesData | null>(null);
  const [_filteredFormation, _setFilteredFormation] = useState<string | null>(
    null
  );

  useEffect(() => {
    if (props.club) {
      const _crosses = formatCrosses(
        props.club,
        PLAYERS_TO_SHOW,
        _filteredFormation
      );

      _setCrossesData(_crosses);
    }
  }, [_filteredFormation, props.club]);

  useEffect(() => {
    _setFilteredFormation(null);
  }, [props.club]);

  /* Listeners */
  function _onClubIconClick(club: BasicClub) {
    props.onIconClick(club);
  }

  function _onFormationFilterChange(value: string | null) {
    _setFilteredFormation(value);
  }

  /* Render */
  if (!_crossesData) return null;
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <ClubButton
          club={props.club as BasicClub}
          onClick={_onClubIconClick}
          showName={false}
          size={"small"}
        />
      </div>
      <div className={styles.content}>
        <div className={styles.left}>
          <div className={styles.title}>{"Crosses"}</div>
          <div className={styles.filter}>
            <div className={styles.filterLabel}>{"Showing all events for"}</div>
            <div className={styles.filterDropdown}>
              <Dropdown
                placeholder="any formation"
                options={_crossesData.formations}
                onChange={_onFormationFilterChange}
                value={_filteredFormation}
              />
            </div>
          </div>
          <div className={styles.players}>
            <PlayersList players={_crossesData.players} />
          </div>
        </div>
        <Pitch players={_crossesData.players} />
      </div>
    </div>
  );
}

export default CrossesOverview;
