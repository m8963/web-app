import { gql } from "@apollo/client";
export const GET_CLUBS = gql`
  query GetClubs {
    clubs {
      club_name
      image
    }
  }
`;

export const GET_CLUB = gql`
  query GetClub($clubName: String, $eventType: EventType) {
    club(club_name: $clubName) {
      club_name
      image
      league
      players {
        image
        player_name
      }
      events(eventType: $eventType) {
        _id
        formation
        player_name
        start_x
        start_y
        type
        match {
          match_id
          away_team
          away_team_score
          home_team
          home_team_score
        }
        pass {
          end_x
          end_y
          is_cross
          receiving_player
        }
      }
    }
  }
`;
