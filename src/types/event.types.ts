import { Match } from "./match.types";
import { PlayerWithCrosses } from "./player.types";

export enum EventType {
  SHOT = "shot",
  PASS = "pass",
}

export enum ShotResult {
  GOAL = "Goal",
  MISS = "Miss",
  SAVED = "Saved",
  ON_TARGET = "On Target",
}

export interface EventPass {
  end_x: number;
  end_y: number;
  is_cross: boolean;
  receiving_player: string;
}

export interface EventShot {
  shot_result: ShotResult;
}

export interface Event {
  _id: string;
  formation: string;
  player_name: string;
  type: EventType;
  match: Match;
  start_x: number;
  start_y: number;
  pass?: EventPass;
  shot?: EventShot;
}

export interface CrossesData {
  formations: string[];
  players: PlayerWithCrosses[];
}

export interface CrossesCountType {
  total: number;
  successful: number;
}

export interface CrossesCountInfo {
  total: CrossesCountType;
  early: CrossesCountType;
  mid: CrossesCountType;
  late: CrossesCountType;
}
