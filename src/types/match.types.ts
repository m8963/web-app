export interface Match {
  match_id: string;
  away_team: string;
  away_team_score: number | null;
  home_team: string;
  home_team_score: number | null;
}
