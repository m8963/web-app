import { Event, EventType } from "./event.types";
import { Player } from "./player.types";

export interface BasicClub {
  club_name: string;
  image: string;
  league: string;
}

export interface Club extends BasicClub {
  players: Player[];
  events: Event[];
}

export interface ClubListData {
  clubs: BasicClub[];
}

export interface ClubData {
  club: Club;
}

export interface GetClubProps {
  club_name: string;
  eventType: string;
}

export interface GET_CLUB_PROPS {
  clubName: string;
  eventType: EventType;
}
