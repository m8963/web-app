import { Event } from "./event.types";

export interface Player {
  club: string;
  image: string;
  player_name: string;
}

export interface PlayerWithCrosses extends Player {
  crosses: Event[];
}
