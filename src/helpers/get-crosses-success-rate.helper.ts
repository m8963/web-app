import { CrossesCountInfo, Event } from "../types/event.types";

import STRINGS from "../config/strings";
import CROSS_TRESHOLDS from "../config/cross-tressholds.config";

function getCrossesSuccessRate(crosses: Event[]): CrossesCountInfo {
  const _tempCrossInfo = {
    total: {
      total: 0,
      successful: 0,
    },
    early: {
      total: 0,
      successful: 0,
    },
    mid: {
      total: 0,
      successful: 0,
    },
    late: {
      total: 0,
      successful: 0,
    },
  };

  crosses.forEach((cross) => {
    _tempCrossInfo.total.total += 1;

    if (cross.pass?.receiving_player !== STRINGS.OPPONENT) {
      _tempCrossInfo.total.successful += 1;
    }

    // There's probably a smarter way to do this
    if (cross.start_y > CROSS_TRESHOLDS.LATE) {
      _tempCrossInfo.late.total += 1;
      if (cross.pass?.receiving_player !== STRINGS.OPPONENT) {
        _tempCrossInfo.late.successful += 1;
      }
    } else if (cross.start_y > CROSS_TRESHOLDS.MID) {
      _tempCrossInfo.mid.total += 1;
      if (cross.pass?.receiving_player !== STRINGS.OPPONENT) {
        _tempCrossInfo.mid.successful += 1;
      }
    } else {
      _tempCrossInfo.early.total += 1;
      if (cross.pass?.receiving_player !== STRINGS.OPPONENT) {
        _tempCrossInfo.early.successful += 1;
      }
    }
  });

  return _tempCrossInfo;
}

export default getCrossesSuccessRate;
