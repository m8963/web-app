import { Club } from "../types/club.types";
import { CrossesData } from "../types/event.types";
import { PlayerWithCrosses } from "../types/player.types";
import STRINGS from "../config/strings";

function _sortPlayers(a: PlayerWithCrosses, b: PlayerWithCrosses): number {
  const _successFulACrosses = a.crosses.filter(
    (cross) => cross.pass?.receiving_player !== STRINGS.OPPONENT
  ).length;

  const _successFulBCrosses = b.crosses.filter(
    (cross) => cross.pass?.receiving_player !== STRINGS.OPPONENT
  ).length;
  return _successFulBCrosses - _successFulACrosses;
}

export function formatCrosses(
  club: Club,
  playersToShow: number,
  formation: string | null
): CrossesData {
  let _tempFormations: string[] = [];
  let _tempPlayers: PlayerWithCrosses[] = [];

  club.events.forEach((event) => {
    if (event.pass?.is_cross) {
      // Get team formations
      if (_tempFormations.indexOf(event.formation) < 0) {
        _tempFormations.push(event.formation);
      }

      // Get top 3 cross takers
      const _tempFormationPlayer = _tempPlayers.find(
        (player) => player.player_name === event.player_name
      );
      if (!_tempFormationPlayer) {
        const _clubPlayer = club.players.find(
          (player) => player.player_name === event.player_name
        );
        if (_clubPlayer && (!formation || event.formation === formation)) {
          _tempPlayers.push({
            ..._clubPlayer,
            crosses: [event],
          });
        }
      } else {
        if (!formation || event.formation === formation) {
          _tempFormationPlayer.crosses.push(event);
        }
      }
    }
  });

  return {
    formations: _tempFormations.sort(),
    players: _tempPlayers
      .sort((a, b) => _sortPlayers(a, b))
      .slice(0, playersToShow),
  };
}
