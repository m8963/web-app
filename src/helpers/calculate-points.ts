import { Event } from "../types/event.types";
import { Dimensions } from "../types/pitch.types";

function calculatePoints(event: Event, dimensions: Dimensions): number[] {
  const _coordinates: number[] = [];

  _coordinates.push(dimensions.width * (event.start_x / 100));
  _coordinates.push(dimensions.height * (event.start_y / 100));
  _coordinates.push(dimensions.width * ((event.pass?.end_x || 0) / 100));
  _coordinates.push(dimensions.height * ((event.pass?.end_y || 0) / 100));

  return _coordinates;
}

export default calculatePoints;
