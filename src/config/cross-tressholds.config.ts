const crossesTreshold = {
  EARLY: 0,
  MID: 70,
  LATE: 90,
};
export default crossesTreshold;
