# My Game Plan Web - Demo

## Project setup

1. Clone this repository
2. Run `yarn`
3. Copy `.env.example` and name it `.env.local`. If you don't have the server repository setup, you can also connect to the live database hosted at `https://mygameplan-staging.herokuapp.com/`
4. Run `yarn start`

## Context

### Fetching and filtering data by ID

You should always use IDs to fetch a single piece of data or filter, but that requires the data to be setup correctly, eg. a player's data shouldn't have a `club_name` property but a `club_id` property. But to prevent opening Pandora's box and completelylosing it while manually editing JSON files, I opted to just use `club_name`, despite knowing it's a big no-no.

### Responsiveness

This project is not responsive at all. It looks okay on larger screens and landscape tablets but on smaller screens, it's awful. I know! Design for mobile devices should be more thought-through so for now, I chose to just leave it.
